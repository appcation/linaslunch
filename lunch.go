package main

import (
	"fmt"
	"math/rand"
	"time"
)

type foodGroup []string
type lunch []string

/*
Purpose: To create a random lunch containing all food groups in it!
Preconditions: Assigned to lunch type variable
Postconditions: A new random lunch is returned
*/

func newLunch() lunch {
	options := lunch{}

	// Create random seed generator
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	// Can possibly let users choose these in the future via menuItem/button/function. bulitin for now
	veggieOptions := foodGroup{"Broccoli", "Cucumbers", "Cauliflower",
		"Carrots", "Corn", "Green beans"}

	fruitOptions := foodGroup{"Apple slices", "Cherries", "Blackberries", "Rasberries", "Grapes"}
	proteinOptions := foodGroup{"Black Beans", "Edamame", "Lunchmeat",
		"Fresh chicken/turkey"}
	grainOptions := foodGroup{"Soup", "Muffin", "Bread", "Rice Cake",
		"Wrap", "Cheerios"}

	snackOptions := foodGroup{"Goldfish", "Crackers", "Apple"}
	sugarFatOptions := foodGroup{"Gummie Snack", "Kellog Fruit Bar", "Avacado"}

	// return options of 1 random thing from each food group

	// Debug Output
	/*
		fmt.Println(r.Intn(len(veggieOptions)))
	*/
	//  End Debug Output

	// Add 1 of each food group
	options = append(options, veggieOptions[r.Intn(len(veggieOptions))])
	options = append(options, fruitOptions[r.Intn(len(fruitOptions))])
	options = append(options, proteinOptions[r.Intn(len(proteinOptions))])
	options = append(options, grainOptions[r.Intn(len(grainOptions))])
	options = append(options, snackOptions[r.Intn(len(snackOptions))])
	options = append(options, sugarFatOptions[r.Intn(len(sugarFatOptions))])

	// return the random healthy lunch
	return options
}

func (f foodGroup) print() {
	for _, food := range f {
		fmt.Println(food)
	}
}

func (l lunch) print() {
	for i, food := range l {

		if i == 0 {
		}
		switch i {
		case (0):
			fmt.Println("Vegetable options are: ")
		case (1):
			fmt.Println("Fruit options are: ")
		case (2):
			fmt.Println("Protein options are: ")
		case (3):
			fmt.Println("Grain options are: ")
		case (4):
			fmt.Println("Snack options are: ")
		case (5):
			fmt.Println("Sugar/Fat options are: ")
		}
		fmt.Println(food)
	}
}
